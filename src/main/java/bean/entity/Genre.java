package bean.entity;

public class Genre extends BaseEntity {
    private String name;

    public Genre(){}

    public Genre(Integer id, String name) {
        super(id);
        this.name = name;
    }

    public Integer getId() {
        return super.getId();
    }

    public void setId(Integer id) {
        super.setId(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Genre)) return false;

        Genre genre = (Genre) o;
        if (!getId().equals(genre.getId())) return false;
        return name.equals(genre.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        return 31 * result +getId().hashCode();
    }

    @Override
    public String
    toString() {
        return "Genre{" +
                "id=" + getId() +
                "name='" + name + '\'' +
                '}';
    }
}
