package command;

import bean.CommandResponse;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {
    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response);
}
