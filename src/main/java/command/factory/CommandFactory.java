package command.factory;

import command.Command;
import command.impl.*;

import java.util.HashMap;
import java.util.Map;

public class CommandFactory {
    public static class SingletonHolder {
        public static final CommandFactory HOLDER_INSTANCE = new CommandFactory();
    }

    public static CommandFactory getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private static final Map<String, Command> commands;
    static {
        commands = new HashMap<String, Command>();
        commands.put("library", new GetLibrary());
        commands.put("add_book", new AddBook());
        commands.put("update_book", new UpdateBook());
        commands.put("delete_book", new DeleteBook());
        commands.put("edit_book", new EditBook());
        commands.put("add_author", new AddAuthor());
        commands.put("update_author", new UpdateAuthor());
        commands.put("delete_author", new DeleteAuthor());
        commands.put("edit_author", new EditAuthor());
        commands.put("add_genre", new AddGenre());
        commands.put("update_genre", new UpdateGenre());
        commands.put("delete_genre", new DeleteGenre());
        commands.put("edit_genre", new EditGenre());
    }

    public Command getCommand(String commandName){
        return commands.get(commandName);
    }
}
