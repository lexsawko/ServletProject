package command.impl;

import bean.entity.Author;
import bean.CommandResponse;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class AddAuthor implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
        Author author = new Author();
        author.setFirstName(request.getParameter("first_name"));
        author.setLastName(request.getParameter("last_name"));
        DaoFactory.getInstance().getAuthorDao().create(author);
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/library");
        return commandResponse;
    }
}
