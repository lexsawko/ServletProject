package command.impl;

import bean.CommandResponse;

import bean.entity.Author;
import bean.entity.Book;
import bean.entity.Genre;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class AddBook implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setPages(Integer.parseInt(request.getParameter("pages")));
        book.setAge(Integer.parseInt(request.getParameter("age")));
        Genre genre = new Genre();
        genre.setId(Integer.valueOf(request.getParameter("genre")));
        book.setGenre(genre);
        Author author = new Author();
        author.setId(Integer.valueOf(request.getParameter("author")));
        book.setAuthor(author);
        DaoFactory.getInstance().getBookDao().create(book);
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/library");
        return commandResponse;
    }
}
