package command.impl;

import bean.CommandResponse;
import command.Command;
import org.apache.log4j.Logger;
import dao.factory.DaoFactory;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class DeleteBook implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
        DaoFactory.getInstance().getBookDao().delete(Integer.parseInt(request.getParameter("id")));
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("library");
        return commandResponse;
    }
}
