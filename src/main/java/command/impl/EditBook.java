package command.impl;

import bean.CommandResponse;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class EditBook implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
            request.setAttribute("genres",DaoFactory.getInstance().getGenreDao().findAll());
            request.setAttribute("authors",DaoFactory.getInstance().getAuthorDao().findAll());
            if(request.getParameter("id")!=null){
            request.setAttribute("method","Edit");
                request.setAttribute("book", DaoFactory.getInstance().getBookDao()
                        .findEntityById(Integer.parseInt(request.getParameter("id"))));
            } else {
                request.setAttribute("method","Add");
            }
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/WEB-INF/views/editBook.jsp");
        return commandResponse;
    }
}
