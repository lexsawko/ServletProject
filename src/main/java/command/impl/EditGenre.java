package command.impl;

import bean.CommandResponse;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;

public class EditGenre implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {

            if(request.getParameter("id")!=null){
                request.setAttribute("method","Edit");
                request.setAttribute("genre", DaoFactory.getInstance().getGenreDao()
                        .findEntityById(Integer.parseInt(request.getParameter("id"))));
            } else {
                request.setAttribute("method","Add");
            }
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/WEB-INF/views/editGenre.jsp");
        return commandResponse;
    }
}
