package command.impl;

import bean.entity.Author;
import bean.entity.Book;
import bean.CommandResponse;
import bean.entity.Genre;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;
import util.DBUtils;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.List;

public class GetLibrary implements Command {
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
        List<Book> books;
        List<Author> authors;
        List<Genre> genres;
        books = DaoFactory.getInstance().getBookDao().findAll();
        request.setAttribute("books",books);
        authors = DaoFactory.getInstance().getAuthorDao().findAll();
        request.setAttribute("authors", authors);
        genres = DaoFactory.getInstance().getGenreDao().findAll();
        request.setAttribute("genres", genres);
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/WEB-INF/views/library.jsp");
        return commandResponse;
    }
}
