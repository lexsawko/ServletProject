package command.impl;

import bean.CommandResponse;
import bean.entity.Genre;
import command.Command;
import dao.factory.DaoFactory;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UpdateGenre implements Command{
    private static Logger logger = Logger.getLogger(AddAuthor.class);

    public CommandResponse execute(HttpServletRequest request, HttpServletResponse response) {
        Genre genre = new Genre();
        genre.setId(Integer.valueOf(request.getParameter("id")));
        genre.setName(request.getParameter("name"));
        DaoFactory.getInstance().getGenreDao().update(genre);
        CommandResponse commandResponse = new CommandResponse();
        commandResponse.setTargetURL("/library");
        return commandResponse;
    }
}
