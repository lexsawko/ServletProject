package controller;
import bean.CommandResponse;
import command.Command;
import command.factory.CommandFactory;
import org.apache.log4j.Logger;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class Controller extends HttpServlet {
    private static Logger logger = Logger.getLogger(Controller.class);

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandName = request.getServletPath().replace("/", "");

        Command command = CommandFactory.getInstance().getCommand(commandName);
        CommandResponse commandResponse = command.execute(request,response);
        logger.debug("Command " + commandName + " was executed");
        request.getRequestDispatcher(commandResponse.getTargetURL()).forward(request,response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String commandName = request.getServletPath().replace("/", "");
        Command command = CommandFactory.getInstance().getCommand(commandName);
        CommandResponse commandResponse = command.execute(request,response);
        logger.debug("Command " + commandName + " was executed");
        response.sendRedirect(commandResponse.getTargetURL());
    }
}
