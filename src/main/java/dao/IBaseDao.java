package dao;

import bean.entity.BaseEntity;

import java.sql.Connection;
import java.util.List;

public interface IBaseDao<T extends BaseEntity> {
    List<T> findAll();
    T findEntityById(Integer id);
    boolean delete(Integer id);
    boolean create(T entity);
    boolean update(T entity);
}
