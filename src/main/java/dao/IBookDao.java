package dao;

import bean.entity.Book;

import java.sql.Connection;
import java.util.List;

public interface IBookDao<T extends Book> extends IBaseDao<T> {
    List<Book> getBooksByAuthorName(String name, String surname);
    Integer getBooksCount();
}
