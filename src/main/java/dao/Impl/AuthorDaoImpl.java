package dao.Impl;

import bean.entity.Author;
import dao.IAuthorDao;
import org.apache.log4j.Logger;
import dao.PSSupplier.AuthorPSSupplier;
import util.DBUtils;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class AuthorDaoImpl implements IAuthorDao<Author>{
    private static Logger logger = Logger.getLogger(AuthorDaoImpl.class);
    private AuthorPSSupplier psSupplier;

    public AuthorDaoImpl(){
        psSupplier = new AuthorPSSupplier();
    }

    @Override
    public List<Author> findAll() {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        final String GET_ALL_AUTHORS = "SELECT * FROM author";
        List<Author> authors = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL_AUTHORS);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Author author = new Author();
                author.setId(resultSet.getInt("idauthor"));
                author.setFirstName(resultSet.getString("first_name"));
                author.setLastName(resultSet.getString("last_name"));
                authors.add(author);
            }
        } catch (SQLException e) {
            logger.error("Find all authors SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return authors;
    }

    @Override
    public Author findEntityById(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        Author author = new Author();
        try (PreparedStatement preparedStatement = psSupplier.getAuthorById(id,conn);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                author.setId(resultSet.getInt("idauthor"));
                author.setFirstName(resultSet.getString("first_name"));
                author.setLastName(resultSet.getString("last_name"));
            }
        } catch (SQLException e) {
            logger.error("Find author by id SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return author;
    }

    @Override
    public boolean delete(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.deleteAuthorPS(id,conn)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Delete author SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean create(Author entity) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.createAuthorPS(entity,conn)) {
            preparedStatement.executeUpdate();
            return true;
        }catch(SQLException e){
            logger.error("Create author SQL error - " + e);
            } finally{
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            return false;
        }

    @Override
    public boolean update(Author entity) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.updateAuthorPS(entity,conn)){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Update author SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
