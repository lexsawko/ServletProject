package dao.Impl;

import bean.entity.Author;
import bean.entity.Book;
import bean.entity.Genre;
import dao.IBookDao;
import org.apache.log4j.Logger;
import dao.PSSupplier.BookPSSupplier;
import util.DBUtils;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements IBookDao<Book> {
    private static Logger logger = Logger.getLogger(BookDaoImpl.class);
    private BookPSSupplier psSupplier;

    public BookDaoImpl(){
        psSupplier = new BookPSSupplier();
    }

    @Override
    public List<Book> getBooksByAuthorName(String name, String surname) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        final String GET_BOOKS = "SELECT books.book_id, books.book_name, books.pages, books.age, genre.genre_name" +
                " FROM books INNER JOIN author ON books.author_id=author.idauthor " +
                "INNER JOIN genre ON books.genre_id=genre.idgenre WHERE author.first_name='" + name + "' AND " +
                "author.last_name='" + surname + "';";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_BOOKS);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setPages(resultSet.getInt("pages"));
                book.setAge(resultSet.getInt("age"));
                book.setGenre(new Genre(resultSet.getInt("idgenre"), resultSet.getString("genre_name")));
                books.add(book);
            }
        } catch (SQLException e) {
            logger.error("Get books by author name SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return books;
    }

    @Override
    public Integer getBooksCount() {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        final String GET_COUNT = "SELECT count(*) as counter FROM books";
        Integer counter=0;
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_COUNT);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            resultSet.next();
            counter = resultSet.getInt("counter");
        } catch (SQLException e) {
            logger.error("Get books count SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return counter;
    }

    @Override
    public List<Book> findAll() {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        final String GET_ALL_BOOKS = "SELECT * FROM books INNER JOIN author ON books.author_id=author.idauthor " +
                "INNER JOIN genre ON books.genre_id=genre.idgenre";
        List<Book> books = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL_BOOKS);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Book book = new Book();
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setPages(resultSet.getInt("pages"));
                book.setAge(resultSet.getInt("age"));
                book.setGenre(new Genre(resultSet.getInt("idgenre"), resultSet.getString("genre_name")));
                book.setAuthor(new Author(resultSet.getInt("idauthor"),
                        resultSet.getString("first_name"), resultSet.getString("last_name")));
                books.add(book);
            }
        } catch (SQLException e) {
            logger.error("Find all books SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return books;
    }



    @Override
    public Book findEntityById(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        Book book = new Book();
        try (PreparedStatement preparedStatement = psSupplier.getBookByIdPS(id, conn);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                book.setId(resultSet.getInt("book_id"));
                book.setName(resultSet.getString("book_name"));
                book.setPages(resultSet.getInt("pages"));
                book.setAge(resultSet.getInt("age"));
                book.setGenre(new Genre(resultSet.getInt("idgenre"), resultSet.getString("genre_name")));
                book.setAuthor(new Author(resultSet.getInt("idauthor"),
                        resultSet.getString("first_name"), resultSet.getString("last_name")));
            }
        } catch (SQLException e) {
            logger.error("Find book by id SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return book;
    }

    @Override
    public boolean delete(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.deleteBookPS(id,conn)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Delete book SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean update(Book book) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.updateBookPS(book,conn)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Update book SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean create(Book book) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.createBookPS(book,conn)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Create book SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    public BookPSSupplier getPsSupplier() {
        return psSupplier;
    }

    public void setPsSupplier(BookPSSupplier psSupplier) {
        this.psSupplier = psSupplier;
    }

}
