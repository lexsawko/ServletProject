package dao.Impl;

import bean.entity.Genre;
import dao.IGenreDao;
import org.apache.log4j.Logger;
import dao.PSSupplier.GenrePSSupplier;
import util.DBUtils;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GenreDaoImpl implements IGenreDao<Genre> {
    private static Logger logger = Logger.getLogger(GenreDaoImpl.class);
    private GenrePSSupplier psSupplier;

    public GenreDaoImpl(){
        psSupplier = new GenrePSSupplier();
    }

    @Override
    public List<Genre> findAll() {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        final String GET_ALL_GENRES = "SELECT * FROM genre";
        List<Genre> genres = new ArrayList<>();
        try (PreparedStatement preparedStatement = conn.prepareStatement(GET_ALL_GENRES);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                Genre genre = new Genre();
                genre.setId(resultSet.getInt("idgenre"));
                genre.setName(resultSet.getString("genre_name"));
                genres.add(genre);
            }
        } catch (SQLException e) {
            logger.error("Find all genres SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return genres;
    }

    @Override
    public Genre findEntityById(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        Genre genre = new Genre();
        try (PreparedStatement preparedStatement = psSupplier.getGenreById(id,conn);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                genre.setId(resultSet.getInt("idgenre"));
                genre.setName(resultSet.getString("genre_name"));
            }
        } catch (SQLException e) {
            logger.error("Find genre by id SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return genre;
    }

    @Override
    public boolean delete(Integer id) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.deleteGenrePS(id,conn)) {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Delete genre SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean create(Genre entity) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.createGenrePS(entity,conn)) {
                preparedStatement.executeUpdate();
                return true;
        }catch(SQLException e){
            logger.error("Create genre SQL error - " + e);
        } finally{
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean update(Genre entity) {
        Connection conn = null;
        try {
            conn = DBUtils.getConnection();
        } catch (NamingException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            logger.error("SQL exception in class " + getClass(), e);
        }

        try (PreparedStatement preparedStatement = psSupplier.updateGenrePS(entity,conn)){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Update genre SQL error - " + e);
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}
