package dao.PSSupplier;



import bean.entity.Author;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class AuthorPSSupplier {

    public PreparedStatement getAuthorById(int authorId, Connection conn) throws SQLException {
        final String GET_AUTHOR_BY_ID = "SELECT * FROM author WHERE idauthor=?";
        PreparedStatement ps = conn.prepareStatement(GET_AUTHOR_BY_ID);
        ps.setInt(1, authorId);
        return ps;
    }

    public PreparedStatement deleteAuthorPS(Integer id, Connection conn) throws SQLException {
        final String DELETE_AUTHOR = "DELETE FROM author WHERE idauthor=?;";
        PreparedStatement ps = conn.prepareStatement(DELETE_AUTHOR);
        ps.setInt(1, id);
        return ps;
    }

    public PreparedStatement updateAuthorPS(Author author, Connection conn) throws SQLException {
        final String UPDATE_AUTOR = "UPDATE author SET first_name=?, last_name=?" +
                "WHERE idauthor=?;";
        PreparedStatement ps = conn.prepareStatement(UPDATE_AUTOR);
        ps.setString(1, author.getFirstName());
        ps.setString(2, author.getLastName());
        ps.setInt(3,author.getId());
        return ps;
    }

    public PreparedStatement createAuthorPS(Author author, Connection conn) throws SQLException {
        final String INSERT_INTO_AUTHOR = "INSERT INTO author (first_name, last_name) VALUES (?, ?);";
        PreparedStatement ps = conn.prepareStatement(INSERT_INTO_AUTHOR);
        ps.setString(1, author.getFirstName());
        ps.setString(2, author.getLastName());
        return ps;
    }
}
