package dao.PSSupplier;



import bean.entity.Book;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BookPSSupplier {

    public PreparedStatement getBookByIdPS(int bookId, Connection conn) throws SQLException {
        final String GET_BOOK_BY_ID = "SELECT * FROM books INNER JOIN author ON books.author_id=author.idauthor " +
                "INNER JOIN genre ON books.genre_id=genre.idgenre WHERE books.book_id=?";
        PreparedStatement ps = conn.prepareStatement(GET_BOOK_BY_ID);
        ps.setInt(1, bookId);
        return ps;
    }

    public PreparedStatement createBookPS(Book book, Connection conn) throws SQLException {
        final String INSERT_INTO_BOOKS = "INSERT INTO books (book_name, pages, age, genre_id, author_id)" +
                "VALUES (?, ?, ?, ?,?);";
        PreparedStatement ps = conn.prepareStatement(INSERT_INTO_BOOKS);
        ps.setString(1, book.getName());
        ps.setInt(2, book.getPages());
        ps.setInt(3, book.getAge());
        ps.setInt(4, book.getGenre().getId());
        ps.setInt(5, book.getAuthor().getId());
        return ps;
    }

    public PreparedStatement updateBookPS(Book book, Connection conn) throws SQLException {
        final String UPDATE_BOOK = "UPDATE books SET book_name=?, pages=?, age=?, genre_id=?, author_id=? " +
                "WHERE book_id=?;";
        PreparedStatement ps = conn.prepareStatement(UPDATE_BOOK);
        ps.setString(1, book.getName());
        ps.setInt(2, book.getPages());
        ps.setInt(3, book.getAge());
        ps.setInt(4, book.getGenre().getId());
        ps.setInt(5, book.getAuthor().getId());
        ps.setInt(6, book.getId());
        return ps;
    }

    public PreparedStatement deleteBookPS(Integer id, Connection conn) throws SQLException {
        final String DELETE_BOOK = "DELETE FROM books WHERE book_id=?;";
        PreparedStatement ps = conn.prepareStatement(DELETE_BOOK);
        ps.setInt(1, id);
        return ps;
    }
}
