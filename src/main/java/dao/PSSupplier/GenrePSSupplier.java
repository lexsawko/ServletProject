package dao.PSSupplier;


import bean.entity.Genre;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class GenrePSSupplier {

    public PreparedStatement getGenreById(int genreId, Connection conn) throws SQLException {
        final String GET_GENRE_BY_ID = "SELECT * FROM genre WHERE idgenre=?";
        PreparedStatement ps = conn.prepareStatement(GET_GENRE_BY_ID);
        ps.setInt(1, genreId);
        return ps;
    }

    public PreparedStatement deleteGenrePS(Integer id, Connection conn) throws SQLException {
        final String DELETE_GENRE = "DELETE FROM genre WHERE idgenre=?;";
        PreparedStatement ps = conn.prepareStatement(DELETE_GENRE);
        ps.setInt(1, id);
        return ps;
    }

    public PreparedStatement updateGenrePS(Genre genre, Connection conn) throws SQLException {
        final String UPDATE_GENRE = "UPDATE genre SET genre_name=? WHERE idgenre=?;";
        PreparedStatement ps = conn.prepareStatement(UPDATE_GENRE);
        ps.setString(1, genre.getName());
        ps.setInt(2,genre.getId());
        return ps;
    }

    public PreparedStatement createGenrePS(Genre genre, Connection conn) throws SQLException {
        final String INSERT_INTO_GENRE = "INSERT INTO genre (genre_name) VALUES  (?);";
        PreparedStatement ps = conn.prepareStatement(INSERT_INTO_GENRE);
        ps.setString(1, genre.getName());
        return ps;
    }
}
