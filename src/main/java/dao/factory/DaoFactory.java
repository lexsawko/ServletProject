package dao.factory;

import dao.Impl.AuthorDaoImpl;
import dao.Impl.BookDaoImpl;
import dao.Impl.GenreDaoImpl;

public class DaoFactory {
    public static class SingletonHolder {
        public static final DaoFactory HOLDER_INSTANCE = new DaoFactory();
    }

    public static DaoFactory getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private AuthorDaoImpl authorDao = new AuthorDaoImpl();
    private BookDaoImpl bookDao = new BookDaoImpl();
    private GenreDaoImpl genreDao = new GenreDaoImpl();

    public AuthorDaoImpl getAuthorDao() {
        return authorDao;
    }

    public BookDaoImpl getBookDao() {
        return bookDao;
    }

    public GenreDaoImpl getGenreDao() {
        return genreDao;
    }
}
