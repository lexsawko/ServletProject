package util;

import dao.Impl.BookDaoImpl;

import javax.naming.NamingException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;

public class BooksTag extends SimpleTagSupport{
    private boolean showBooks;
    private StringWriter sw = new StringWriter();

    public void setShowBooks(boolean showBooks){
        this.showBooks = showBooks;
    }

    @Override
    public void doTag() throws JspException, IOException {
        if(showBooks){
                getJspBody().invoke(sw);
                getJspContext().getOut()
                        .println(sw + new BookDaoImpl().getBooksCount().toString());
        }
    }
}
