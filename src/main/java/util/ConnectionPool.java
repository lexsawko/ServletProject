package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ConnectionPool {
    private Properties properties;
    private String connectionUrl;
    private int minPoolSize;
    private int maxPoolSize;
    private float loadFactor;
    private int incrementPortion;
    private int idleTimeLimit;
    private ArrayBlockingQueue<ConnectionWrapper> connectionsQueue;
    private int currentPoolSize;

    private void startCleaner(Thread mainThread){
        new Thread(()-> {
            while(mainThread.isAlive()){
                try {
                Thread.sleep(idleTimeLimit/2);
                    for(ConnectionWrapper wrapper : connectionsQueue){
                        if(System.currentTimeMillis()-wrapper.creationTime.getTime()>=idleTimeLimit){
                           ConnectionPool.getInstance().deleteConnection(wrapper);
                        }
                    }
                } catch (InterruptedException  e){
                }
            }
    }).start();
    }

    private class ConnectionWrapper{
        private Connection connection;
        private Date creationTime;

        public ConnectionWrapper(){}

        public ConnectionWrapper(Connection conn){
            connection = conn;
            idleTimeLimit = ConnectionPool.getInstance().idleTimeLimit;
            creationTime = new Date();
        }
    }

    public static class SingletonHolder {
        public static final ConnectionPool HOLDER_INSTANCE = new ConnectionPool();
    }

    public static ConnectionPool getInstance() {
        return SingletonHolder.HOLDER_INSTANCE;
    }

    private void offerConnections(int amount){
        ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()+1);
        for(int i=0; i<amount;i++) {
            try {
                connectionsQueue.offer(executorService.submit(()->{
                        Connection conn = DriverManager.getConnection(connectionUrl, properties);
                        return ConnectionPool.getInstance().new ConnectionWrapper(conn);
                }).get());
            } catch (Exception e) {
                System.err.println("Connection error" + e);
            }
        }
        executorService.shutdown();
        currentPoolSize += amount;
    }

    public void configuratePool(PoolConfig config){
        properties = config.getProperties();
        connectionUrl = "jdbc:mysql://" + config.getHost() + ":" + config.getPort() + "/"
                + config.getDbName();
        minPoolSize = config.getMinPoolSize();
        maxPoolSize = config.getMaxPoolSize();
        loadFactor = config.getLoadFactor();
        incrementPortion = config.getIncrementPortion();
        idleTimeLimit = config.getIdleTimeLimit();
        connectionsQueue = new ArrayBlockingQueue<>(maxPoolSize);
        offerConnections(minPoolSize);
        startCleaner(Thread.currentThread());
    }

    public Connection getConnecton(){
        ConnectionWrapper wrapper = null;
        if(connectionsQueue.size() <= currentPoolSize*(1-loadFactor)){
            if(currentPoolSize+incrementPortion>maxPoolSize){
                offerConnections(maxPoolSize-currentPoolSize);
                System.out.println("pool increased to max= " + maxPoolSize);
            }else{
                offerConnections(incrementPortion);
                System.out.println("pool increased to " + currentPoolSize);
            }
        }
        try {
            wrapper = connectionsQueue.take();
            System.out.println("connection was taken, free= " + connectionsQueue.size());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return wrapper.connection;
    }

    public void returnConnection(Connection connection){
        connectionsQueue.offer(ConnectionPool.getInstance().new ConnectionWrapper(connection));
        System.out.println("connection was return, free= " + connectionsQueue.size());
    }

    public void deleteConnection(ConnectionWrapper connectionWrapper) {
        connectionsQueue.remove(connectionWrapper);
        currentPoolSize--;
        System.out.println("connection died, conn left " + connectionsQueue.size());
    }
}
