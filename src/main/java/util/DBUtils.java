package util;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DBUtils {
    private static Context ctx;
    private static DataSource dataSource;

    public static Connection getConnection() throws NamingException, SQLException{
        if(ctx == null){
            ctx = (Context) (new InitialContext().lookup("java:comp/env"));
        }
        if(dataSource == null){
            dataSource = (DataSource) ctx.lookup("jdbc/TestDB");
        }
        return dataSource.getConnection();
    }
}
