package util;

import java.util.Properties;

public class PoolConfig {
    private Properties prop;
    private String host;
    private int port;
    private String dbName;
    private int minPoolSize;
    private int maxPoolSize;
    private float loadFactor;
    private int incrementPortion;
    private int idleTimeLimit;

    public PoolConfig(){
        prop = new Properties();
        prop.put("user", "root");
        prop.put("password", "1234");
        prop.put("autoReconnect", "true");
        prop.put("serverTimezone", "UTC");
        prop.put("useSSL", "false");
        prop.put("characterEncoding", "UTF-8");
        prop.put("useUnicode", "true");
        setDbName("library");
        setHost("localhost");
        setPort(3306);
        setIdleTimeLimit(3000);
        setIncrementPortion(4);
        setLoadFactor(0.8F);
        setMinPoolSize(10);
        setMaxPoolSize(20);
    }

    public int getIdleTimeLimit() {
        return idleTimeLimit;
    }

    public void setIdleTimeLimit(int idleTimeLimit) {
        this.idleTimeLimit = idleTimeLimit;
    }

    public Properties getProperties() {
        return prop;
    }

    public void setProperties(Properties properties) {
        this.prop = properties;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public int getMinPoolSize() {
        return minPoolSize;
    }

    public void setMinPoolSize(int minPoolSize) {
        this.minPoolSize = minPoolSize;
    }

    public int getMaxPoolSize() {
        return maxPoolSize;
    }

    public void setMaxPoolSize(int maxPoolSize) {
        this.maxPoolSize = maxPoolSize;
    }

    public float getLoadFactor() {
        return loadFactor;
    }

    public void setLoadFactor(float loadFactor) {
        this.loadFactor = loadFactor;
    }

    public int getIncrementPortion() {
        return incrementPortion;
    }

    public void setIncrementPortion(int incrementPortion) {
        this.incrementPortion = incrementPortion;
    }
}
