<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
    <title>${method} author</title>
</head>
<body>
<h1>${method} author</h1>
<c:choose>
    <c:when test="${method eq 'Edit'}">
<form action="update_author" method="post">
    <table class="margin">
        <tr>
            <th>Id</th>
            <td><input type="number" name="id" placeholder="ID" value="${author.id}" readonly></td>
        </tr>
    </c:when>
        <c:otherwise>
        <form action="add_author" method="post">
            <table class="margin">
        </c:otherwise>
</c:choose>
        <tr>
            <th>First Name</th>
            <td><input type="text" name="first_name" placeholder="First name" value="${author.firstName}" required></td>
        </tr>
        <tr>
            <th>Last Name</th>
            <td><input type="text" name="last_name" min="0" placeholder="Last Name" value="${author.lastName}" required></td>
        </tr>
    </table>
    <button type="submit" class="btn-save">Save</button>
</form>
</body>
</html>
