<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
    <title>${method} book</title>
</head>
<body>
<h1>${method} book</h1>
<c:choose>
    <c:when test="${method eq 'Edit'}">
        <form action="update_book" method="post">
            <table class="margin">
                <tr>
                    <th>Id</th>
                    <td><input type="number" name="id" placeholder="book_id" value="${book.id}" readonly></td>
                </tr>
                <tr>
                    <th>Name</th>
                    <td><input type="text" name="name" placeholder="book_name" value="${book.name}" required></td>
                </tr>
                <tr>
                    <th>Pages</th>
                    <td><input type="number" name="pages" min="0" placeholder="pages" value="${book.pages}" required></td>
                </tr>
                <tr>
                    <th>Age</th>
                    <td><input type="number" name="age" min="0" placeholder="age" value="${book.age}" required></td>
                </tr>
                <tr>
     </c:when>
     <c:otherwise>
         <form action="add_book" method="post">
            <table class="margin">
                <tr>
                    <th>Name</th>
                    <td><input type="text" name="name" placeholder="book_name" required></td>
                </tr>
                <tr>
                    <th>Pages</th>
                    <td><input type="number" name="pages" placeholder="pages" min="0" required></td>
                </tr>
                <tr>
                    <th>Age</th>
                    <td><input type="number" name="age" placeholder="age" min="0" required></td>
                </tr>
     </c:otherwise>
</c:choose>
                <tr>
                    <th>Genre</th>
                    <td>
                        <select name="genre">
                            <c:forEach var="Genre" items="${genres}">
                                <option value="${Genre.id}" ${book.genre.id == Genre.id ? 'selected':''}>${Genre.name}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th>Author</th>
                    <td>
                        <select name="author">
                            <c:forEach var="Author" items="${authors}">
                                <option value="${Author.id}" ${book.author.id==Author.id ? 'selected':''}>${Author.firstName} ${Author.lastName}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
            </table>
            <button type="submit" class="btn-save">Save</button>
        </form>
</body>
</html>
