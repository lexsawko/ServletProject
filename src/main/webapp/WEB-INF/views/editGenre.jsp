<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
    <title>${method} genre</title>
</head>
<body>
<h1>${method} genre</h1>
<c:choose>
<c:when test="${method eq 'Edit'}">
<form action="update_genre" method="post">
    <table class="margin">
        <tr>
            <th>Id</th>
            <td><input type="number" name="id" placeholder="ID" value="${genre.id}" readonly></td>
        </tr>
        </c:when>
        <c:otherwise>
        <form action="add_genre" method="post">
            <table class="margin">
                </c:otherwise>
                </c:choose>
                <tr>
                    <th>Name</th>
                    <td><input type="text" name="name" placeholder="Name" value="${genre.name}" required></td>
                </tr>
            </table>
            <button type="submit" class="btn-save">Save</button>
        </form>
</body>
</html>
