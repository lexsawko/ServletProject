<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="x" uri="http://localhost:8080"%>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
    <title>Welcome</title>
</head>
<body>
<section>
    <h1>Welcome to the library!</h1>
    <a href="library"><button class="button" type="submit">Show library</button></a>
</section>
</body>
</html>